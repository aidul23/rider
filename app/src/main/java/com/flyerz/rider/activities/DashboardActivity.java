package com.flyerz.rider.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.bumptech.glide.Glide;
import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.flyerz.rider.R;
import com.flyerz.rider.adapter.SliderAdapter;
import com.flyerz.rider.callbacks.OnRiderCallback;
import com.flyerz.rider.databinding.ActivityDashboardBinding;
import com.flyerz.rider.model.Rider;
import com.flyerz.rider.utility.Utility;
import com.flyerz.rider.viewmodel.DashboardActivityViewModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.ListenerRegistration;
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

public class DashboardActivity extends AppCompatActivity implements OnRiderCallback, View.OnClickListener {
    private static final String TAG = "DashboardActivity";
    private ActivityDashboardBinding binding;
    private ListenerRegistration registration;
    private CFAlertDialog alertDialog;
    private SliderView sliderView;
    private final EventListener<DocumentSnapshot> riderDocumentEventListener = (value, error) -> {
        if (value != null) {

            if (TextUtils.equals("Pending Confirmation", value.get("riderStatus").toString())) {

                alertDialog = new CFAlertDialog.Builder(this)
                        .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                        .setTitle("Pending Approval")
                        .setMessage("Your registration process has been completed. Right now your application is being verified by the admins. You will be notified shortly.")
                        .addButton("OK", -1, -1, CFAlertDialog.CFAlertActionStyle.DEFAULT, CFAlertDialog.CFAlertActionAlignment.JUSTIFIED, (dialog, which) -> {
                            registration.remove();
                            dialog.dismiss();
                        }).create();

                binding.riderStatusSwitch.setVisibility(View.GONE);

                binding.offlineRequest.setVisibility(View.INVISIBLE);

                alertDialog.show();

            } else if (TextUtils.equals("Approved", value.get("riderStatus").toString())) {

                alertDialog = new CFAlertDialog.Builder(this)
                        .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                        .setTitle("Approval Confirmed")
                        .setMessage("Your application is approved. You are now an official rider of Flyerz. You will be given orders shortly. Thank you for joining us.")
                        .addButton("OK", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.JUSTIFIED, (dialog, which) -> {
                            Utility.setHasUserAcknowledgedApprovalToTrue();
                            goOnlineOffline("Available");
                            registration.remove();
                            dialog.dismiss();
                        }).create();

                binding.riderStatusSwitch.setVisibility(View.VISIBLE);

                binding.offlineRequest.setVisibility(View.VISIBLE);

                alertDialog.show();

            }
        }
    };

    private DashboardActivityViewModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityDashboardBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        model = new ViewModelProvider(this).get(DashboardActivityViewModel.class);


        int[] images = {R.drawable.one,
                R.drawable.two,
                R.drawable.three};
        SliderAdapter sliderAdapter = new SliderAdapter(images);
        binding.imageSlider.setSliderAdapter(sliderAdapter);
        binding.imageSlider.setIndicatorAnimation(IndicatorAnimationType.WORM);
        binding.imageSlider.setIndicatorAnimation(IndicatorAnimationType.WORM);
        binding.imageSlider.setSliderTransformAnimation(SliderAnimations.DEPTHTRANSFORMATION);
        binding.imageSlider.startAutoCycle();

        binding.profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DashboardActivity.this, ProfileActivity.class);
                startActivity(intent);
            }
        });

        binding.pendingCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DashboardActivity.this, MainActivity.class).putExtra("orderState","Pending"));
            }
        });

        //aidul
        binding.deliveredCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DashboardActivity.this, MainActivity.class).putExtra("orderState","Delivered"));
            }
        });

        binding.riderStatusSwitch.setEnabled(false);
        binding.riderStatusSwitch.setLabelOff("Offline");
        binding.riderStatusSwitch.setLabelOn("Online");
        binding.riderStatusSwitch.setColorDisabled(getColor(R.color.color_light));

        binding.offlineRequest.setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (!Utility.getHasUserAcknowledgedApproval()) {

            registration = FirebaseFirestore.getInstance()
                    .collection("RIDER")
                    .document(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber())
                    .addSnapshotListener(riderDocumentEventListener);
        }

        model.getRider(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (registration != null) {
            registration.remove();
        }
    }

    @Override
    public void onRider(Rider rider) {
        Glide.with(DashboardActivity.this)
                .load(rider.getRiderImage())
                .into(binding.profile);
    }

    @Override
    public void onRiderStatus(boolean isOnline) {
        binding.riderStatusSwitch.setOn(isOnline);
        binding.offlineRequest.setText(isOnline ? "Offline Request" : "Online Request");
    }

    @Override
    public void onRiderAvailabilityRequest(boolean hasRequestedToLeave, boolean hasApprovedToLeave) {

        if (hasRequestedToLeave) {
            alertDialog = new CFAlertDialog.Builder(this)
                    .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
                    .setTitle("Pending Approval")
                    .setMessage("You have requested to go offline. Your request is being reviewed. You will be notified shortly.")
                    .addButton("OK", -1, -1, CFAlertDialog.CFAlertActionStyle.DEFAULT, CFAlertDialog.CFAlertActionAlignment.JUSTIFIED, (dialog, which) -> {
                        dialog.dismiss();
                    }).create();
        }

        if (hasApprovedToLeave) {
            alertDialog = new CFAlertDialog.Builder(this)
                    .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                    .setTitle("Request Approved")
                    .setMessage("Your request to go offline has been approved.")
                    .addButton("OK", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.JUSTIFIED, (dialog, which) -> {
                        dialog.dismiss();
                    }).create();

            goOnlineOffline("Offline");
        }

        alertDialog.show();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == binding.offlineRequest.getId()) {
            if (TextUtils.equals(binding.offlineRequest.getText(), "Offline Request")) {
                requestToGoOffline();
            } else {
                binding.offlineRequest.setText("Offline Request");
                binding.riderStatusSwitch.setOn(true);
                goOnlineOffline("Available");
            }
        }
    }

    private void requestToGoOffline() {
        FirebaseFirestore.getInstance().collection("RIDER")
                .document(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber())
                .update("hasRequestedToLeave", true);
    }

    private void goOnlineOffline(String status) {

        FirebaseFirestore.getInstance().collection("RIDER")
                .document(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber())
                .update("riderStatus", status);

        if (TextUtils.equals("Available", status)) {

            FirebaseFirestore.getInstance().collection("RIDER")
                    .document(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber())
                    .update("hasRequestedToLeave", false);

            FirebaseFirestore.getInstance().collection("RIDER")
                    .document(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber())
                    .update("hasApprovedToLeave", false);
        }
    }
}