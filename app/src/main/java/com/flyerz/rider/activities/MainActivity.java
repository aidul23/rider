package com.flyerz.rider.activities;

import android.Manifest;
import android.content.Intent;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.flyerz.rider.constants.Constants;
import com.flyerz.rider.databinding.ActivityMainBinding;
import com.flyerz.rider.viewmodel.MainActivityViewModel;
import com.vmadalin.easypermissions.EasyPermissions;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class MainActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks {

    private boolean isPermissionGranted;

    private MainActivityViewModel model;

    private AlertDialog enbleGpsAlrtDialog;

    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        model = new ViewModelProvider(this).get(MainActivityViewModel.class);

        if (hasLocationPermission()) {
            isPermissionGranted = true;
        } else {
            requestLocationPermission();
        }

        isGPSenable();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull @NotNull String[] permissions, @NonNull @NotNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    private boolean hasLocationPermission() {
        return EasyPermissions.hasPermissions(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
    }

    private void requestLocationPermission() {
        EasyPermissions.requestPermissions(
                this,
                "This app requires location permission",
                Constants.PERMISSION_LOCATION_REQUEST_CODE,
                Manifest.permission.ACCESS_FINE_LOCATION
        );
    }

    @Override
    public void onPermissionsDenied(int i, @NotNull List<String> list) {
        if (EasyPermissions.somePermissionDenied(this, list.get(0))) {
            Intent intent = new Intent();
            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            Uri uri = Uri.fromParts("package", getPackageName(), "");
            intent.setData(uri);
            startActivity(intent);
        } else {
            requestLocationPermission();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.GPS_REQUEST_CODE) {
            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            boolean providerEnable = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            if (providerEnable) enbleGpsAlrtDialog.dismiss();
        }
    }

    @Override
    public void onPermissionsGranted(int i, @NotNull List<String> list) {
        isPermissionGranted = true;
        Toast.makeText(this, "Permission granted", Toast.LENGTH_SHORT).show();
    }

    private boolean isGPSenable() {
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        boolean providerEnable = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (providerEnable) {
            return true;
        } else {
            enbleGpsAlrtDialog = new AlertDialog.Builder(this).setTitle("GPS Permission")
                    .setMessage("Please Enable GPS")
                    .setPositiveButton("OK", ((dialogInterface, i) -> {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivityForResult(intent, Constants.GPS_REQUEST_CODE);
                    })).setCancelable(true).show();
        }
        return false;
    }

    @Override
    public void onBackPressed() {

        if (model.getIsDeliveryStarted().getValue() && model.getIsDeliveryFinished().getValue()) {
            super.onBackPressed();
        } else if (!model.getIsDeliveryStarted().getValue()) {
            super.onBackPressed();
        } else {
            Toast.makeText(this, "Sorry, you have an ongoing delivery", Toast.LENGTH_LONG).show();
        }

    }
}