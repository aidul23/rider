package com.flyerz.rider.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.firebase.ui.auth.AuthUI;
import com.flyerz.rider.databinding.ActivityProfileBinding;
import com.flyerz.rider.utility.Utility;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import org.jetbrains.annotations.NotNull;

public class ProfileActivity extends AppCompatActivity {

    private ActivityProfileBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityProfileBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AuthUI.getInstance().signOut(ProfileActivity.this)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull @NotNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Intent intent = new Intent(ProfileActivity.this, AuthActivity.class);
                                    startActivity(intent);
                                    Utility.clearSharedPreferences();
                                    finish();
                                } else {
                                    Toast.makeText(ProfileActivity.this, "" + task.getException(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        getRiderAdditionalDetails();
    }

    private void getRiderAdditionalDetails() {
        FirebaseFirestore.getInstance().collection("RIDER")
                .document(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber())
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if (documentSnapshot.exists()) {

                            documentSnapshot.getReference()
                                    .collection("additional information")
                                    .document(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber())
                                    .get()
                                    .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                        @Override
                                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                                            if (documentSnapshot.exists()) {

                                                binding.riderName.setText(
                                                        Utility.rider.getRiderName()
                                                );

                                                binding.riderCategory.setText(
                                                        Utility.rider.getRiderCategory()
                                                );

                                                binding.riderPhoneNumber.setText(
                                                        Utility.rider.getRiderPhoneNumber()
                                                );

                                                Glide.with(ProfileActivity.this).load(Utility.rider.getRiderImage()).into(binding.riderImage);

                                                binding.riderDocumentNo.setText(
                                                        documentSnapshot.get("riderDocumentNo").toString()
                                                );

                                                binding.riderPresentAddress.setText(
                                                        documentSnapshot.get("riderPresentAddress").toString()
                                                );

                                                binding.riderVehicleNo.setText(
                                                        documentSnapshot.get("riderVehicleNo").toString()
                                                );
                                            }
                                        }
                                    });

                        }
                    }
                });
    }
}