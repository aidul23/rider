package com.flyerz.rider.activities;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;

import com.bumptech.glide.Glide;
import com.flyerz.rider.R;
import com.flyerz.rider.constants.Constants;
import com.flyerz.rider.databinding.ActivityRegisterBinding;
import com.flyerz.rider.utility.Utility;
import com.flyerz.rider.viewmodel.RegisterActivityViewModel;
import com.flyerz.rider.worker.UploadWorker;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.vmadalin.easypermissions.EasyPermissions;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import id.zelory.compressor.Compressor;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener, Observer<WorkInfo>, CompoundButton.OnCheckedChangeListener {

    private AlertDialog alertDialog;
    private ActivityRegisterBinding binding;
    //Map<String, Object> riderMap = new HashMap<String, String>();
    Map<String, Object> riderMap = new HashMap<>();
    FirebaseFirestore firestore = FirebaseFirestore.getInstance();
    String phone = FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber();
    public static String currentFocusOn;

    public Bitmap profilePicBitMap, nidBackBitMap, nidFrontBitMap, passportBitMap, drivingLcnsFront, drivingLcnsBack;

    public Data dataForProfileUploadWorker,
            dataForNidFrontUploadWorker,
            dataForNidBackUploadWorker,
            dataForPassportUploadWorker,
            dataForDrivingLncsFrontWorker,
            dataForDrivingLncsBackWorker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityRegisterBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());


        RegisterActivityViewModel model = new ViewModelProvider(this).get(RegisterActivityViewModel.class);

        binding.circularImageViewPictureProfile.setOnClickListener(this);
        binding.circularImageViewNidBack.setOnClickListener(this);
        binding.circularImageViewNidFront.setOnClickListener(this);
        binding.drivingLicBack.setOnClickListener(this);
        binding.drivingLicFront.setOnClickListener(this);
        binding.passport.setOnClickListener(this);
        binding.buttonProceed.setOnClickListener(this);

        binding.bicycleRadioButton.setOnCheckedChangeListener(this);

        //binding.phoneNumber.setText(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber());
//        binding.phoneNumber.setText(phone);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (resultCode == RESULT_OK && result != null) {

                Uri resultUri = result.getUri();

                setBitMapAccordingToFocus(resultUri);

                switch (currentFocusOn) {

                    case "profile":
                        Glide.with(this).load(profilePicBitMap).into(binding.circularImageViewPictureProfile);
                        break;

                    case "back":
                        Glide.with(this).load(nidBackBitMap).into(binding.circularImageViewNidBack);
                        break;

                    case "front":
                        Glide.with(this).load(nidFrontBitMap).into(binding.circularImageViewNidFront);
                        break;

                    case "passport":
                        Glide.with(this).load(passportBitMap).into(binding.passport);
                        break;

                    case "license_back":
                        Glide.with(this).load(drivingLcnsBack).into(binding.drivingLicBack);
                        break;

                    case "license_front":
                        Glide.with(this).load(drivingLcnsFront).into(binding.drivingLicFront);
                        break;
                }
            }

        } else {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show();
        }

        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.circularImageView_picture_profile:
                openGalleryAndCropImage();
                currentFocusOn = "profile";
                break;

            case R.id.circularImageView_nid_back:
                openGalleryAndCropImage();
                currentFocusOn = "back";
                break;

            case R.id.circularImageView_nid_front:
                openGalleryAndCropImage();
                currentFocusOn = "front";
                break;

            case R.id.passport:
                openGalleryAndCropImage();
                currentFocusOn = "passport";
                break;

            case R.id.driving_lic_back:
                openGalleryAndCropImage();
                currentFocusOn = "license_back";
                break;

            case R.id.driving_lic_front:
                openGalleryAndCropImage();
                currentFocusOn = "license_front";
                break;

            case R.id.button_proceed:
                chainWorksAndProceed();
                break;
        }
    }

    public void setBitMapAccordingToFocus(Uri resultUri) {

        switch (currentFocusOn) {

            case "profile":
                profilePicBitMap = compressImage(resultUri.getPath());
                dataForProfileUploadWorker = makeDataForWorkerClasses(profilePicBitMap, "profile");
                break;

            case "back":
                nidBackBitMap = compressImage(resultUri.getPath());
                dataForNidBackUploadWorker = makeDataForWorkerClasses(nidBackBitMap, "back");
                break;

            case "front":
                nidFrontBitMap = compressImage(resultUri.getPath());
                dataForNidFrontUploadWorker = makeDataForWorkerClasses(nidFrontBitMap, "front");
                break;

            case "passport":
                passportBitMap = compressImage(resultUri.getPath());
                dataForPassportUploadWorker = makeDataForWorkerClasses(passportBitMap, "passport");
                break;

            case "license_back":
                drivingLcnsBack = compressImage(resultUri.getPath());
                dataForDrivingLncsBackWorker = makeDataForWorkerClasses(drivingLcnsBack, "license_back");
                break;

            case "license_front":
                drivingLcnsFront = compressImage(resultUri.getPath());
                dataForDrivingLncsFrontWorker = makeDataForWorkerClasses(drivingLcnsFront, "license_front");
                break;
        }
    }

    private void chainWorksAndProceed() {

        if (binding.editTextName.getText().toString().trim().equals("")) {
            Toast.makeText(this, "Fill all the field", Toast.LENGTH_SHORT).show();
        } else if (binding.fatherName.getText().toString().trim().equals("")) {
            Toast.makeText(this, "Fill all the field", Toast.LENGTH_SHORT).show();
        } else if (binding.motherName.getText().toString().trim().equals("")) {
            Toast.makeText(this, "Fill all the field", Toast.LENGTH_SHORT).show();
        } else if (binding.presentAddress.getText().toString().trim().equals("")) {
            Toast.makeText(this, "Fill all the field", Toast.LENGTH_SHORT).show();
        } else if (binding.permanentAddress.getText().toString().trim().equals("")) {
            Toast.makeText(this, "Fill all the field", Toast.LENGTH_SHORT).show();
        } else if (binding.lastWork.getText().toString().trim().equals("")) {
            Toast.makeText(this, "Fill all the field", Toast.LENGTH_SHORT).show();
        } else if (binding.workDuration.getText().toString().trim().equals("")) {
            Toast.makeText(this, "Fill all the field", Toast.LENGTH_SHORT).show();
        } else if (binding.workExperience.getText().toString().trim().equals("")) {
            Toast.makeText(this, "Fill all the field", Toast.LENGTH_SHORT).show();
        } else {

            showProgressDialog();

            setRiderInfoOnDb();

            riderMap.put("riderFatherName", binding.fatherName.getText().toString());
            riderMap.put("riderMotherName", binding.motherName.getText().toString());
            riderMap.put("riderPresentAddress", binding.presentAddress.getText().toString());
            riderMap.put("riderPermanentAddress", binding.permanentAddress.getText().toString());
            riderMap.put("riderDrivingNo", binding.drivingNo.getText().toString());
            riderMap.put("riderVehicleNo", binding.vehicleNo.getText().toString());
            riderMap.put("riderDocumentNo", binding.documentNo.getText().toString());
            riderMap.put("riderLastWork", binding.lastWork.getText().toString());
            riderMap.put("riderWorkDuration", binding.workDuration.getText().toString());

            riderMap.put("riderNidBack", "");
            riderMap.put("riderNidFront", "");
            riderMap.put("riderPassport", "");
            riderMap.put("riderLicenseBack", "");
            riderMap.put("riderLicenseFront", "");
            riderMap.put("riderVehicleType", binding.bicycleRadioButton.isChecked() ? "Bicycle" : "Bike");

            firestore.collection("RIDER").document(phone).collection("additional information")
                    .document(phone)
                    .set(riderMap);

            OneTimeWorkRequest uploadProfileRequest = new OneTimeWorkRequest.Builder(UploadWorker.class)
                    .setInputData(dataForProfileUploadWorker)
                    .build();

            OneTimeWorkRequest uploadNidFrontRequest = new OneTimeWorkRequest.Builder(UploadWorker.class)
                    .setInputData(dataForNidFrontUploadWorker)
                    .build();

            OneTimeWorkRequest uploadNidBackRequest = new OneTimeWorkRequest.Builder(UploadWorker.class)
                    .setInputData(dataForNidBackUploadWorker)
                    .build();

            OneTimeWorkRequest uploadPassportRequest = new OneTimeWorkRequest.Builder(UploadWorker.class)
                    .setInputData(dataForPassportUploadWorker)
                    .build();


            WorkManager.getInstance()
                    .enqueue(Arrays.asList(
                            uploadProfileRequest, uploadNidFrontRequest, uploadNidBackRequest,
                            uploadPassportRequest
                    ));


            if (binding.bikeRadioButton.isChecked()) {

                OneTimeWorkRequest uploadDrivingLcnsFrontRequest = new OneTimeWorkRequest.Builder(UploadWorker.class)
                        .setInputData(dataForDrivingLncsFrontWorker)
                        .build();

                OneTimeWorkRequest uploadDrivingLcnsBackRequest = new OneTimeWorkRequest.Builder(UploadWorker.class)
                        .setInputData(dataForDrivingLncsBackWorker)
                        .build();

                WorkManager.getInstance().enqueue(Arrays.asList(
                        uploadDrivingLcnsFrontRequest, uploadDrivingLcnsBackRequest
                ));
            }


            LiveData<WorkInfo> workInfoListenableFuture =
                    WorkManager.getInstance().getWorkInfoByIdLiveData(uploadNidBackRequest.getId());

            workInfoListenableFuture.observe(this, this);

        }

    }

    private void setRiderInfoOnDb() {
        //String phoneNumber = FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber();
        String phoneNumber = phone;
        Map<String, Object> hashMap = new HashMap<>();
        hashMap.put("riderName", binding.editTextName.getText().toString());
        hashMap.put("riderCategory", binding.foodRadioButton.isChecked() ? "Food" : "Parcel");
        hashMap.put("riderStatus", "Pending Confirmation");
        hashMap.put("riderPhoneNumber", phoneNumber);
        hashMap.put("hasRequestedToLeave", false);
        hashMap.put("hasApprovedToLeave", false);
        Utility.riderCollectionReference.document(phoneNumber).set(hashMap);
    }

    @Override
    public void onChanged(WorkInfo workInfo) {
        if (workInfo != null) {
            if (workInfo.getState() == WorkInfo.State.SUCCEEDED) {
                hideProgressDialog();
                Intent intent = new Intent(RegisterActivity.this, DashboardActivity.class);
                startActivity(intent);
                finish();
            }
        }
    }

    private Data makeDataForWorkerClasses(Bitmap bitmap, String s) {
        Data.Builder builder = new Data.Builder();
        builder.putString(Constants.UPLOAD_WORKER_KEY, s);
        builder.putString(Constants.BITMAP_STRING_KEY, Utility.serializeToJson(bitmap));
        return builder.build();
    }

    private Bitmap compressImage(String directoryPath) {
        Bitmap compressedImage = null;
        if (directoryPath.isEmpty() || directoryPath == null) return compressedImage;
        try {
            String extr = Environment.getExternalStorageDirectory().toString();

            //File imageFile = new File(extr+uri);
            File imageFile = new File(directoryPath);
            compressedImage = new Compressor(getApplicationContext())
                    .compressToBitmap(imageFile);

        } catch (IOException e) {
            Log.d("CompressImage Error", e.getMessage());
        }

        return compressedImage;
    }

    public void openGalleryAndCropImage() {
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(this);
    }

    public void showProgressDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        alertDialog = builder.setTitle("Loading")
                .setCancelable(false)
                .setMessage("Finishing registration please wait")
                .create();

        alertDialog.show();

    }

    public void hideProgressDialog() {
        alertDialog.dismiss();
    }

    public boolean hasStoragePermissions() {
        return EasyPermissions.hasPermissions(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) && EasyPermissions.hasPermissions(this, Manifest.permission.READ_EXTERNAL_STORAGE);
    }

    public void requestStoragePermissions() {
        EasyPermissions.requestPermissions(
                this,
                "This app requires storage permission",
                Constants.PERMISSION_WRITE_EXTERNAL_REQUEST_CODE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        );
        EasyPermissions.requestPermissions(
                this,
                "This app requires storage permission",
                Constants.PERMISSION_READ_EXTERNAL_REQUEST_CODE,
                Manifest.permission.READ_EXTERNAL_STORAGE
        );
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        binding.drivingLicFront.setVisibility(b ? View.INVISIBLE : View.VISIBLE);
        binding.drivingLicBack.setVisibility(b ? View.INVISIBLE : View.VISIBLE);

        binding.textDrivingLicFront.setVisibility(b ? View.INVISIBLE : View.VISIBLE);
        binding.textDrivingLicBack.setVisibility(b ? View.INVISIBLE : View.VISIBLE);

        binding.drivingNo.setVisibility(b ? View.GONE : View.VISIBLE);
        binding.vehicleNo.setVisibility(b ? View.GONE : View.VISIBLE);
        binding.documentNo.setVisibility(b ? View.GONE : View.VISIBLE);
    }
}