package com.flyerz.rider.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.flyerz.rider.R;
import com.flyerz.rider.callbacks.OnRiderRegistrationStatusCallback;
import com.flyerz.rider.utility.Utility;
import com.google.firebase.auth.FirebaseAuth;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splsh);

        Handler handler = new Handler();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                if (FirebaseAuth.getInstance().getCurrentUser() != null) {

                    Utility.getRiderRegistrationStatus(new OnRiderRegistrationStatusCallback() {
                        @Override
                        public void isRiderRegistered(boolean isRegistered) {

                            if (isRegistered) {
                                startActivity(
                                        new Intent(SplashActivity.this, DashboardActivity.class)
                                );
                            } else {
                                startActivity(
                                        new Intent(SplashActivity.this, AuthActivity.class)
                                );
                                Toast.makeText(SplashActivity.this, "Please finish the registration process", Toast.LENGTH_SHORT).show();
                            }

                            finish();

                        }
                    });


                } else {
                    startActivity(
                            new Intent(SplashActivity.this, AuthActivity.class)
                    );

                    finish();
                }

            }
        }, 2000);
    }
}