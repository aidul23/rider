package com.flyerz.rider.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.flyerz.rider.R;
import com.flyerz.rider.clicklistener.OnItemClickListener;
import com.flyerz.rider.model.ParcelDeliveryOrder;
import com.flyerz.rider.utility.Utility;

import org.jetbrains.annotations.NotNull;


public class ParcelOrderAdapter extends FirestoreRecyclerAdapter<ParcelDeliveryOrder, ParcelOrderAdapter.ParcelHolder> {
    private static final String TAG = "ParcelOrderAdapter";
    private OnItemClickListener onItemClickListener;

    public void setOrderDetailsItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public ParcelOrderAdapter(@NonNull @NotNull FirestoreRecyclerOptions<ParcelDeliveryOrder> options) {
        super(options);
    }

    @NonNull
    @NotNull
    @Override
    public ParcelHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.parcel_order_item_layout_2, parent, false);
        return new ParcelHolder(view);
    }

    @Override
    protected void onBindViewHolder(@NonNull @NotNull ParcelHolder holder, int position, @NonNull @NotNull ParcelDeliveryOrder model) {
        if (model.getOrderCategory().contains("Food")) {
            holder.orderId.setText("Food : F" + model.getOrderId().substring(0, 6));
        } else {
            holder.orderId.setText("Parcel : P" + model.getOrderId().substring(0, 6));
        }

        Log.d(TAG, "onBindViewHolder: " + model.getOrderId());

        holder.orderCategory.setText(model.getOrderCategory());
        holder.totalCharge.setText("৳ " + model.getTotalCharge());
        holder.orderDateTime.setText(Utility.formatMillisecondsIntoDateAndTime(Long.parseLong(model.getTimeStamp())));
        holder.totalDistance.setText(model.getOrderDistance());
        holder.pickUpAddress.setText(model.getPickupAddress());
        holder.deliveryStatus.setText(model.getOrderStatus());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClickListener.onClick(model);
            }
        });
    }


    public static class ParcelHolder extends RecyclerView.ViewHolder {
        TextView orderId, orderDateTime, pickUpAddress, dropOffAddress, totalCharge, deliveryStatus, totalDistance, orderCategory;

        public ParcelHolder(@NonNull @NotNull View itemView) {
            super(itemView);

            orderId = itemView.findViewById(R.id.order_id);
            orderDateTime = itemView.findViewById(R.id.order_date_time);
            pickUpAddress = itemView.findViewById(R.id.pickup_location);
            dropOffAddress = itemView.findViewById(R.id.destination_address);
            totalCharge = itemView.findViewById(R.id.order_total_cost);
            totalDistance = itemView.findViewById(R.id.total_distance);
            deliveryStatus = itemView.findViewById(R.id.delivery_status);
            orderCategory = itemView.findViewById(R.id.order_category);
        }
    }
}
