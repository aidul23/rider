package com.flyerz.rider.base;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.flyerz.rider.constants.Constants;
import com.vmadalin.easypermissions.EasyPermissions;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class BaseActivity extends AppCompatActivity {

}
