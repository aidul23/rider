package com.flyerz.rider.base;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.flyerz.rider.R;
import com.flyerz.rider.callbacks.OnBottomSheetInteractionCallback;
import com.flyerz.rider.model.ParcelDeliveryOrder;
import com.flyerz.rider.utility.Utility;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import org.jetbrains.annotations.NotNull;

import static android.content.Context.ACTIVITY_SERVICE;

public class BaseFragment extends Fragment implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {
    private static final String TAG = "BaseFragment";
    private ParcelDeliveryOrder order;

    private OnBottomSheetInteractionCallback onBottomSheetInteractionCallback;

    public LatLng destinationLatLng, pickUpLatLng;

    public BottomSheetBehavior bottomSheetBehavior;

    public ScrollView bottomSheetScrollView;

    private LinearLayout timelineLayout;
    private CheckBox checkBox1, checkBox2, checkBox3, checkBox4;
    private TextView orderCategory, orderId, senderAddress, senderName, receiverAddress, receiverName, additionalDirections, callSender, callReceiver, finishDelivery, reportProblem, orderPlacedAt, orderDeliveredAt, totalDistance, totalCost, paymentStatus;


    public void setParcelOrderDelivery(ParcelDeliveryOrder order) {
        if (this.order == null) {
            this.order = order;
            destinationLatLng = Utility.getLatLngFromAddress(order.getDestinationAddress());
            pickUpLatLng = Utility.getLatLngFromAddress(order.getPickupAddress());
        }
    }

    public void setBottomSheetViews() {
        orderCategory = bottomSheetScrollView.findViewById(R.id.order_category);
        orderId = bottomSheetScrollView.findViewById(R.id.order_id);
        timelineLayout = bottomSheetScrollView.findViewById(R.id.layout_1);
        checkBox1 = bottomSheetScrollView.findViewById(R.id.checkbox1);
        checkBox2 = bottomSheetScrollView.findViewById(R.id.checkbox2);
        checkBox3 = bottomSheetScrollView.findViewById(R.id.checkbox3);
        checkBox4 = bottomSheetScrollView.findViewById(R.id.checkbox4);
        totalDistance = bottomSheetScrollView.findViewById(R.id.total_distance);
        totalCost = bottomSheetScrollView.findViewById(R.id.total_cost);
        paymentStatus = bottomSheetScrollView.findViewById(R.id.payment_status);
        senderAddress = bottomSheetScrollView.findViewById(R.id.sender_address);
        receiverAddress = bottomSheetScrollView.findViewById(R.id.receiver_address);
        senderName = bottomSheetScrollView.findViewById(R.id.sender_name);
        receiverName = bottomSheetScrollView.findViewById(R.id.receiver_name);
        additionalDirections = bottomSheetScrollView.findViewById(R.id.additional_direction);
        callSender = bottomSheetScrollView.findViewById(R.id.call_sender);
        callReceiver = bottomSheetScrollView.findViewById(R.id.call_receiver);
        finishDelivery = bottomSheetScrollView.findViewById(R.id.finish_delivery);
        reportProblem = bottomSheetScrollView.findViewById(R.id.report_problem);
        orderDeliveredAt = bottomSheetScrollView.findViewById(R.id.order_deliveredAt);
        orderPlacedAt = bottomSheetScrollView.findViewById(R.id.order_placedAt);
    }

    public void bindDataToBottomSheet() {
        orderCategory.setText(order.getOrderCategory());
        orderId.setText("Invoice No : " + order.getOrderId().substring(0,6));
        checkBox1.setOnCheckedChangeListener(this);
        checkBox1.setChecked(true);
        checkBox2.setOnCheckedChangeListener(this);
        checkBox3.setOnCheckedChangeListener(this);
        checkBox4.setOnCheckedChangeListener(this);
        callSender.setOnClickListener(this);
        callReceiver.setOnClickListener(this);
        finishDelivery.setOnClickListener(this);
        reportProblem.setOnClickListener(this);
        totalCost.setText(order.getTotalCharge() + " BDT");
        totalDistance.setText(order.getOrderDistance());
        setPaymentStatus();
        senderName.setText(order.getSenderName());
        senderAddress.setText(order.getPickupAddress());
        receiverAddress.setText(order.getDestinationAddress());
        receiverName.setText(order.getReceiverName());
        additionalDirections.setText(order.getAdditionalDirections());
        orderPlacedAt.setText(
                Utility.formatMillisecondsIntoDateAndTime(
                        Long.parseLong(order.getTimeStamp())
                )
        );
    }

    private void setPaymentStatus() {
        FirebaseFirestore.getInstance().collection("PARCEL")
                .document(order.getDocumentId())
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        documentSnapshot.getReference().collection("payment status")
                                .document(order.getDocumentId())
                                .get()
                                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                    @Override
                                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                                        if (documentSnapshot.exists()) {

                                            paymentStatus.setText(documentSnapshot.getBoolean("paid") ? "Paid" : "Pending");

                                        }
                                    }
                                });
                    }
                });
    }

    public void setOnBottomSheetInteractionCallback(OnBottomSheetInteractionCallback callback) {
        this.onBottomSheetInteractionCallback = callback;
    }

    @Override
    public void onStart() {
        super.onStart();

        timelineLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                bottomSheetBehavior.setPeekHeight(timelineLayout.getHeight() + 48);
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                bottomSheetBehavior.addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                    @Override
                    public void onStateChanged(@NonNull @NotNull View bottomSheet, int newState) {
                        if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                            bottomSheetScrollView.smoothScrollTo(0, 0);
                        }
                    }

                    @Override
                    public void onSlide(@NonNull @NotNull View bottomSheet, float slideOffset) {

                    }
                });
            }
        });
    }

    public boolean isLocationServiceRunning() {
        ActivityManager manager = (ActivityManager) requireActivity().getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if ("com.flyerz.rider.service.LocationService".equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.call_receiver:
                onBottomSheetInteractionCallback.onCallReceiver(order.getReceiverNumber());
                break;
            case R.id.call_sender:
                onBottomSheetInteractionCallback.onCallSender(order.getSenderNumber());
                break;
            case R.id.finish_delivery:
                onBottomSheetInteractionCallback.onFinishDelivery();
                break;
            case R.id.report_problem:
                onBottomSheetInteractionCallback.onReportProblem();
                break;
        }
    }

    public void call(String receiverNumber) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + receiverNumber));
        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
            Log.d(TAG, "call: "+getActivity().getPackageManager());
            startActivity(intent);
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (buttonView.getId() == R.id.checkbox1 && isChecked) {
            onBottomSheetInteractionCallback.onParcelOrderAccepted();
        } else if (buttonView.getId() == R.id.checkbox2 && isChecked) {
            onBottomSheetInteractionCallback.onParcelPickedUp();
        } else if (buttonView.getId() == R.id.checkbox3 && isChecked) {
            onBottomSheetInteractionCallback.onParcelDeliveryOngoing();
        } else if (buttonView.getId() == R.id.checkbox4 && isChecked) {
            onBottomSheetInteractionCallback.onParcelDelivered();
        }
    }

    public boolean isDeliveredChecked() {
        return checkBox4.isChecked();
    }
}
