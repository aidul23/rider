package com.flyerz.rider.callbacks;

public interface OnBottomSheetInteractionCallback {
    void onParcelOrderAccepted();

    void onParcelPickedUp();

    void onParcelDeliveryOngoing();

    void onParcelDelivered();

    void onCallSender(String number);

    void onCallReceiver(String number);

    void onReportProblem();

    void onFinishDelivery();
}
