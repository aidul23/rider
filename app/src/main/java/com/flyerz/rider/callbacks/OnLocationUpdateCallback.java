package com.flyerz.rider.callbacks;

import android.location.Location;

public interface OnLocationUpdateCallback {
    void onLocationUpdate(Location location);
}
