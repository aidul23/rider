package com.flyerz.rider.callbacks;

import com.flyerz.rider.model.Rider;

public interface OnRiderCallback {
    void onRider(Rider rider);

    void onRiderStatus(boolean isOnline);

    void onRiderAvailabilityRequest(boolean hasRequestedToLeave, boolean hasApprovedToLeave);
}
