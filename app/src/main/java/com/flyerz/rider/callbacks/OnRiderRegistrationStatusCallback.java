package com.flyerz.rider.callbacks;

public interface OnRiderRegistrationStatusCallback {
    void isRiderRegistered(boolean isRegistered);
}
