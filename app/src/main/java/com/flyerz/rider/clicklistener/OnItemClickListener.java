package com.flyerz.rider.clicklistener;

import com.flyerz.rider.model.ParcelDeliveryOrder;

public interface OnItemClickListener {
    public void onClick(ParcelDeliveryOrder parcelDeliveryOrder);
}
