package com.flyerz.rider.constants;

public class Constants {
    public static int GPS_REQUEST_CODE = 9001;
    public static int PERMISSION_LOCATION_REQUEST_CODE = 200;
    public static int PERMISSION_READ_EXTERNAL_REQUEST_CODE = 100;
    public static int PERMISSION_WRITE_EXTERNAL_REQUEST_CODE = 100;
    public static final String SELECTED_PARCEL_TYPE_TO_QUERY = "selected-parcel-type-to-query";
    public static String START_FOREGROUND_SERVICE = "start-foreground-service";
    public static String STOP_FOREGROUND_SERVICE = "stop-foreground-service";
    public static String SERVICE_RESULT_RECEIVER = "service-result-receiver";
    public static String RIDER_LOCATION_KEY = "rider-location-key";
    public static String UPLOAD_WORKER_KEY = "upload-worker-key";
    public static String STORAGE_RIDER = "RIDER_PHOTOS";
    public static String BITMAP_STRING_KEY = "bitmap-string-key";
    public static String SHARED_PREFERENCES_KEY = "shared-preferences-key";
    public static String IS_USER_ACKNOWLEDGED_APPROVAL_KEY = "approval-acknowledge-key";
}
