package com.flyerz.rider.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.flyerz.rider.R;
import com.flyerz.rider.base.BaseFragment;
import com.flyerz.rider.callbacks.OnBottomSheetInteractionCallback;
import com.flyerz.rider.constants.Constants;
import com.flyerz.rider.databinding.FragmentDetailsBinding;
import com.flyerz.rider.resultreceiver.ServiceResultReceiver;
import com.flyerz.rider.service.LocationService;
import com.flyerz.rider.viewmodel.MainActivityViewModel;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import org.jetbrains.annotations.NotNull;

public class DetailsFragment extends BaseFragment implements OnMapReadyCallback, ServiceResultReceiver.Receiver, OnBottomSheetInteractionCallback {

    private static final String TAG = "DetailsFragment";

    private MainActivityViewModel model;
    private FragmentDetailsBinding binding;
    private GoogleMap googleMap;
    private SupportMapFragment supportMapFragment;
    private Marker riderMarker, destinationMarker, pickUpMarker;
    private ServiceResultReceiver mReceiver;
    private FusedLocationProviderClient mLocationClient;
    private CFAlertDialog alertDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        model = new ViewModelProvider(requireActivity()).get(MainActivityViewModel.class);
        setParcelOrderDelivery(model.getParcelDeliveryOrderMutableLiveData().getValue());
        mReceiver = new ServiceResultReceiver(new Handler());
        mReceiver.setReceiver(this);
        mLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity());
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentDetailsBinding.inflate(getLayoutInflater());
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        bottomSheetScrollView = view.findViewById(R.id.bottom_sheet);
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheetScrollView);
        setOnBottomSheetInteractionCallback(this);
        setBottomSheetViews();
        bindDataToBottomSheet();
    }

    @Override
    public void onActivityCreated(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        FragmentManager fm = getActivity().getSupportFragmentManager();
        supportMapFragment = (SupportMapFragment) fm.findFragmentById(R.id.map_view);
        if (supportMapFragment == null) {
            supportMapFragment = SupportMapFragment.newInstance();
            fm.beginTransaction().replace(R.id.map_view, supportMapFragment).commit();
        }
        supportMapFragment.getMapAsync(this);

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(@NonNull @NotNull GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        this.googleMap.setMyLocationEnabled(true);
        addDestinationMarker(destinationLatLng.latitude, destinationLatLng.longitude);
        addPickUpMarker(pickUpLatLng.latitude, pickUpLatLng.longitude);
        //addDestinationMarker(22.331964, 91.823452);
        getRiderCurrentLocation();
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        if (resultCode == 100 && resultData != null) {
            Location location = resultData.getParcelable(Constants.RIDER_LOCATION_KEY);
            LatLng newLocation = new LatLng(location.getLatitude(), location.getLongitude());
            Log.d(TAG, "onReceiveResult: " + newLocation.toString());
            riderMarker.setPosition(newLocation);
            setMapBoundsOnFirstLocationUpdate();
        }
    }

    @SuppressLint("MissingPermission")
    private void getRiderCurrentLocation() {
        mLocationClient.getLastLocation()
                .addOnSuccessListener(new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if (location != null) {
                            addRiderCurrentLocationMarker(location.getLatitude(), location.getLongitude());
                        }
                    }
                });
    }

    private void addRiderCurrentLocationMarker(double latitude, double longitude) {
        LatLng latLng = new LatLng(latitude, longitude);
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Rider");
        riderMarker = googleMap.addMarker(markerOptions);
        setMapBoundsOnFirstLocationUpdate();
    }

    private void addDestinationMarker(double latitude, double longitude) {
        LatLng latLng = new LatLng(latitude, longitude);
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 18);
        googleMap.animateCamera(cameraUpdate);
        destinationMarker = googleMap.addMarker(new MarkerOptions().position(latLng).title("Dropoff"));
    }

    private void addPickUpMarker(double latitude, double longitude) {
        LatLng latLng = new LatLng(latitude, longitude);
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 18);
        googleMap.animateCamera(cameraUpdate);
        pickUpMarker = googleMap.addMarker(new MarkerOptions().position(latLng).title("Pickup"));
    }

    private void setMapBoundsOnFirstLocationUpdate() {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(pickUpMarker.getPosition());
        builder.include(destinationMarker.getPosition());
        builder.include(riderMarker.getPosition());
        LatLngBounds bounds = builder.build();
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 200);
        googleMap.animateCamera(cu);
    }

    private void startForegroundService(String action) {
        // if not running create one
        if (!isLocationServiceRunning()) {
            Intent serviceIntent = new Intent(requireActivity(), LocationService.class);
            serviceIntent.setAction(Constants.START_FOREGROUND_SERVICE);
            serviceIntent.putExtra(Constants.SERVICE_RESULT_RECEIVER, mReceiver);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                getActivity().startForegroundService(serviceIntent);
            } else {
                getActivity().startService(serviceIntent);
            }
        } else {
            // if running stop that
            Intent serviceIntent = new Intent(requireActivity(), LocationService.class);
            serviceIntent.setAction(action);
            serviceIntent.putExtra(Constants.SERVICE_RESULT_RECEIVER, mReceiver);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                getActivity().startForegroundService(serviceIntent);
            } else {
                getActivity().startService(serviceIntent);
            }
        }
    }

    @Override
    public void onParcelOrderAccepted() {
        startForegroundService(Constants.START_FOREGROUND_SERVICE);
        model.setParcelOrderAccepted();
    }

    @Override
    public void onParcelPickedUp() {
        model.setParcelOrderPickedUp();
    }

    @Override
    public void onParcelDeliveryOngoing() {
        model.setParcelOrderOngoing();
    }

    @Override
    public void onParcelDelivered() {

        model.setParcelOrderDelivered();
    }

    @Override
    public void onCallSender(String number) {
        call(number);
    }

    @Override
    public void onCallReceiver(String number) {
        call(number);
    }

    @Override
    public void onReportProblem() {

    }

    @Override
    public void onFinishDelivery() {
        alertDialog = new CFAlertDialog.Builder(requireContext())
                .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
                .setTitle("Delivery Confirmation")
                .setMessage("Are you sure want to finish delivery?")
                .addButton("YES", -1, -1, CFAlertDialog.CFAlertActionStyle.DEFAULT, CFAlertDialog.CFAlertActionAlignment.JUSTIFIED, (dialog, which) -> {
                    dialog.dismiss();
                }).create();
        alertDialog.show();
        startForegroundService("stop");
        model.setParcelOrderDeliveredAt();
    }
}