package com.flyerz.rider.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.flyerz.rider.adapter.ParcelOrderAdapter;
import com.flyerz.rider.clicklistener.OnItemClickListener;
import com.flyerz.rider.databinding.FragmentHomeBinding;
import com.flyerz.rider.model.ParcelDeliveryOrder;
import com.flyerz.rider.utility.Utility;
import com.flyerz.rider.viewmodel.MainActivityViewModel;

import org.jetbrains.annotations.NotNull;


public class HomeFragment extends Fragment {

    private static final String TAG = "HomeFragment";

    private FragmentHomeBinding binding;
    private NavController navController;
    private ParcelOrderAdapter parcelOrderAdapter;
    private MainActivityViewModel model;
    private String orderState;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        model = new ViewModelProvider(requireActivity()).get(MainActivityViewModel.class);

        orderState = requireActivity().getIntent().getStringExtra("orderState");
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentHomeBinding.inflate(getLayoutInflater());
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        navController = Navigation.findNavController(view);

        FirestoreRecyclerOptions<ParcelDeliveryOrder> options = null;
        if(orderState.equals("Pending")){
            options =
                    new FirestoreRecyclerOptions.Builder<ParcelDeliveryOrder>()
                            .setQuery(Utility.getQuery("Order Placed"), ParcelDeliveryOrder.class)
                            .build();
        } else if(orderState.equals("Delivered")){
            options =
                    new FirestoreRecyclerOptions.Builder<ParcelDeliveryOrder>()
                            .setQuery(Utility.getQuery("Delivered"), ParcelDeliveryOrder.class)
                            .build();
        }


        parcelOrderAdapter = new ParcelOrderAdapter(options);
        parcelOrderAdapter.notifyDataSetChanged();
        binding.homeFragmentRecyclerView.setAdapter(parcelOrderAdapter);

        parcelOrderAdapter.setOrderDetailsItemClickListener(new OnItemClickListener() {
            @Override
            public void onClick(ParcelDeliveryOrder parcelDeliveryOrder) {
                if (parcelDeliveryOrder.getOrderStatus().equals("Delivered")) {
                    Toast.makeText(requireContext(), "This parcel is already delivered", Toast.LENGTH_SHORT).show();
                    return;
                }
                model.getParcelDeliveryOrderMutableLiveData().setValue(parcelDeliveryOrder);
                navController.navigate(HomeFragmentDirections.actionHomeFragmentToDetailsFragment());
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        parcelOrderAdapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        parcelOrderAdapter.startListening();
    }
}