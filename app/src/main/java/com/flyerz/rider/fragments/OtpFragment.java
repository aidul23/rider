package com.flyerz.rider.fragments;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;

import com.flyerz.rider.activities.DashboardActivity;
import com.flyerz.rider.activities.RegisterActivity;
import com.flyerz.rider.callbacks.OnRiderRegistrationStatusCallback;
import com.flyerz.rider.databinding.FragmentOtpBinding;
import com.flyerz.rider.utility.Utility;
import com.flyerz.rider.viewmodel.AuthActivityViewModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import org.jetbrains.annotations.NotNull;

public class OtpFragment extends Fragment {

    private FragmentOtpBinding binding;
    private AuthActivityViewModel model;
    private FirebaseAuth mAuth;
    private NavController navController;

    @Override
    public void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        model = new ViewModelProvider(requireActivity()).get(AuthActivityViewModel.class);
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentOtpBinding.inflate(getLayoutInflater());
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        binding.otpView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 6) {
                    hideKeyboardFrom();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        binding.btnVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(binding.otpView.getText().toString())) {
                    verifyCode(model.getVerificationCode(), binding.otpView.getText().toString());
                } else {
                    Toast.makeText(requireContext(), "Please enter the code and proceed", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void verifyCode(String verificationID, String code) {
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationID, code);
        signInWithCredential(credential);
    }

    private void signInWithCredential(PhoneAuthCredential phoneAuthCredential) {
        mAuth.signInWithCredential(phoneAuthCredential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull @NotNull Task<AuthResult> task) {
                if (task.isSuccessful() && task.getResult() != null) {

                    Utility.getRiderRegistrationStatus(new OnRiderRegistrationStatusCallback() {
                        @Override
                        public void isRiderRegistered(boolean isRegistered) {

                            if (isRegistered) {
                                startActivity(
                                        new Intent(requireActivity(), DashboardActivity.class)
                                );
                            } else {
                                startActivity(
                                        new Intent(requireActivity(), RegisterActivity.class)
                                );
                            }

                            requireActivity().finish();
                        }
                    });
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull @NotNull Exception e) {
                Toast.makeText(requireContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void hideKeyboardFrom() {
        InputMethodManager imm = (InputMethodManager) requireActivity().getSystemService(Service.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(binding.otpView.getWindowToken(), 0);
    }
}