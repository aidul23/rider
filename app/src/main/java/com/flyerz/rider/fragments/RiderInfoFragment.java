package com.flyerz.rider.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.work.OneTimeWorkRequest;

import com.flyerz.rider.R;
import com.flyerz.rider.databinding.FragmentRiderInfoBinding;
import com.flyerz.rider.utility.Utility;
import com.flyerz.rider.viewmodel.RegisterActivityViewModel;
import com.flyerz.rider.worker.UploadWorker;
import com.google.firebase.auth.FirebaseAuth;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

public class RiderInfoFragment extends Fragment implements View.OnClickListener {

    private RegisterActivityViewModel model;
    private FragmentRiderInfoBinding binding;
    private Map<String, Object> riderMap = new HashMap<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        model = new ViewModelProvider(requireActivity()).get(RegisterActivityViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentRiderInfoBinding.inflate(getLayoutInflater());
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.buttonProceed.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.circularImageView_picture_profile) {
            openGalleryAndCropImage();
            return;
        }

        if (binding.editTextName.getText().toString().trim().equals("")) {
            Toast.makeText(requireContext(), "Fill all the field", Toast.LENGTH_SHORT).show();
        } else if (binding.fatherName.getText().toString().trim().equals("")) {
            Toast.makeText(requireContext(), "Fill all the field", Toast.LENGTH_SHORT).show();
        } else if (binding.motherName.getText().toString().trim().equals("")) {
            Toast.makeText(requireContext(), "Fill all the field", Toast.LENGTH_SHORT).show();
        } else if (binding.presentAddress.getText().toString().trim().equals("")) {
            Toast.makeText(requireContext(), "Fill all the field", Toast.LENGTH_SHORT).show();
        } else if (binding.permanentAddress.getText().toString().trim().equals("")) {
            Toast.makeText(requireContext(), "Fill all the field", Toast.LENGTH_SHORT).show();
        } else if (binding.drivingNo.getText().toString().trim().equals("")) {
            Toast.makeText(requireContext(), "Fill all the field", Toast.LENGTH_SHORT).show();
        } else if (binding.vehicleNo.getText().toString().trim().equals("")) {
            Toast.makeText(requireContext(), "Fill all the field", Toast.LENGTH_SHORT).show();
        } else if (binding.documentNo.getText().toString().trim().equals("")) {
            Toast.makeText(requireContext(), "Fill all the field", Toast.LENGTH_SHORT).show();
        } else if (binding.lastWork.getText().toString().trim().equals("")) {
            Toast.makeText(requireContext(), "Fill all the field", Toast.LENGTH_SHORT).show();
        } else if (binding.workDuration.getText().toString().trim().equals("")) {
            Toast.makeText(requireContext(), "Fill all the field", Toast.LENGTH_SHORT).show();
        } else if (binding.workExperience.getText().toString().trim().equals("")) {
            Toast.makeText(requireContext(), "Fill all the field", Toast.LENGTH_SHORT).show();
        } else {

            riderMap.put("riderFatherName", binding.fatherName.getText().toString());
            riderMap.put("riderMotherName", binding.motherName.getText().toString());
            riderMap.put("riderPresentAddress", binding.presentAddress.getText().toString());
            riderMap.put("riderPermanentAddress", binding.permanentAddress.getText().toString());
            riderMap.put("riderDrivingNo", binding.drivingNo.getText().toString());
            riderMap.put("riderVehicleNo", binding.vehicleNo.getText().toString());
            riderMap.put("riderDocumentNo", binding.documentNo.getText().toString());
            riderMap.put("riderLastWork", binding.lastWork.getText().toString());
            riderMap.put("riderWorkDuration", binding.workDuration.getText().toString());
            riderMap.put("riderWorkExperience", binding.workExperience.getText().toString());


            setRiderInfoOnDb();

        }
    }

    public void openGalleryAndCropImage() {
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(requireActivity());
    }

    private void setRiderInfoOnDb() {
        String phoneNumber = FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber();
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("riderName", binding.editTextName.getText().toString());
        //todo hashMap.put("riderCategory", ) rider category selection spinner
        hashMap.put("riderCategory", "General");
        hashMap.put("riderStatus", "pending confirmation");
        hashMap.put("riderPhoneNumber", phoneNumber);
        Utility.riderCollectionReference.document(phoneNumber).set(hashMap);
        Utility.riderCollectionReference.document(phoneNumber).collection("additional information").document(phoneNumber).set(riderMap);
    }
}