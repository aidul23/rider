package com.flyerz.rider.model;

public class LocationStatus {

    private String riderLatitude, riderLongitude;

    public LocationStatus() {
    }


    public LocationStatus(String riderLatitude, String riderLongitude) {
        this.riderLatitude = riderLatitude;
        this.riderLongitude = riderLongitude;
    }

    public double getRiderLatitude() {
        return Double.parseDouble(riderLatitude);
    }

    public double getRiderLongitude() {
        return Double.parseDouble(riderLongitude);
    }
}
