package com.flyerz.rider.model;

public class Rider {

    private boolean hasApprovedToLeave, hasRequestedToLeave;

    private String riderCategory, riderImage, riderName, riderPhoneNumber, riderStatus;

    public Rider() {
    }

    public Rider(boolean hasApprovedToLeave, boolean hasRequestedToLeave, String riderCategory, String riderImage, String riderName, String riderPhoneNumber, String riderStatus) {
        this.hasApprovedToLeave = hasApprovedToLeave;
        this.hasRequestedToLeave = hasRequestedToLeave;
        this.riderCategory = riderCategory;
        this.riderImage = riderImage;
        this.riderName = riderName;
        this.riderPhoneNumber = riderPhoneNumber;
        this.riderStatus = riderStatus;
    }

    public String getRiderCategory() {
        return riderCategory;
    }

    public String getRiderImage() {
        return riderImage;
    }

    public String getRiderName() {
        return riderName;
    }

    public String getRiderPhoneNumber() {
        return riderPhoneNumber;
    }

    public String getRiderStatus() {
        return riderStatus;
    }

    public boolean isHasApprovedToLeave() {
        return hasApprovedToLeave;
    }

    public boolean isHasRequestedToLeave() {
        return hasRequestedToLeave;
    }
}
