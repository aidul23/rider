package com.flyerz.rider.myapp;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.SharedPreferences;
import android.location.Geocoder;
import android.os.Build;

import com.flyerz.rider.R;
import com.flyerz.rider.constants.Constants;
import com.google.android.libraries.places.api.Places;
import com.google.firebase.FirebaseApp;

import java.util.Locale;

public class MyApp extends Application {

    private static Geocoder geocoder;

    private static SharedPreferences sharedPreferences;

    public static SharedPreferences getSharedPreferences() {
        return sharedPreferences;
    }

    public static Geocoder getGeocoder() {
        return geocoder;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        sharedPreferences = getSharedPreferences(Constants.SHARED_PREFERENCES_KEY, MODE_PRIVATE);

        geocoder = new Geocoder(this, Locale.ENGLISH);

        FirebaseApp.initializeApp(this);

        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), getString(R.string.api_key), Locale.forLanguageTag("bn_BD"));
        }

        createNotificationChannel();
    }

    private void createNotificationChannel() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            CharSequence name = getString(R.string.channel_name);

            String description = getString(R.string.channel_description);

            NotificationChannel channel = new NotificationChannel(getString(R.string.location_service_notification_id), name, NotificationManager.IMPORTANCE_HIGH);

            channel.setDescription(description);

            NotificationManager notificationManager = getSystemService(NotificationManager.class);

            notificationManager.createNotificationChannel(channel);
        }
    }
}
