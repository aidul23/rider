package com.flyerz.rider.service;

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.os.ResultReceiver;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;

import com.flyerz.rider.R;
import com.flyerz.rider.constants.Constants;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import org.jetbrains.annotations.NotNull;

public class LocationService extends Service {

    private static final String TAG = "LocationService";
    private final static long UPDATE_INTERVAL = 4 * 1000;  /* 4 secs */
    private FusedLocationProviderClient mFusedLocationClient;
    private ResultReceiver mReceiver;
    private Bundle bundle;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        if (Build.VERSION.SDK_INT >= 26) {
            NotificationCompat.Builder notification =
                    new NotificationCompat.Builder(this, getResources().getString(R.string.location_service_notification_id))
                            .setContentTitle("You have accepted the order")
                            .setContentText("Deliver the parcel to the recipient")
                            .setSmallIcon(R.mipmap.ic_launcher_round);

            startForeground(1, notification.build());
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand: called.");

        if (intent.getAction().equals(Constants.START_FOREGROUND_SERVICE)) {
            mReceiver = intent.getParcelableExtra(Constants.SERVICE_RESULT_RECEIVER);
            getLocation();
        } else {
            stopForeground(true);
            stopSelf();
        }

        return START_NOT_STICKY;
    }

    private void getLocation() {

        // Create the location request to start receiving updates
        LocationRequest mLocationRequestHighAccuracy = new LocationRequest();
        mLocationRequestHighAccuracy.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequestHighAccuracy.setInterval(UPDATE_INTERVAL);


        // new Google API SDK v11 uses getFusedLocationProviderClient(this)
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "from service : permission not given");
            stopSelf();
            return;
        }


        mFusedLocationClient.requestLocationUpdates(mLocationRequestHighAccuracy, new LocationCallback() {
                    @Override
                    public void onLocationResult(@NotNull LocationResult locationResult) {

                        Log.d(TAG, "onLocationResult: got location result." + locationResult.getLastLocation().toString());

                        bundle = new Bundle();

                        bundle.putParcelable(Constants.RIDER_LOCATION_KEY, locationResult.getLastLocation());

                        mReceiver.send(100, bundle);

//                        Location location = locationResult.getLastLocation();
//
//                        updateRiderLocationOnSenderDoc(location);
                    }
                },
                Looper.myLooper()); // Looper.myLooper tells this to repeat forever until thread is destroyed
    }

    private void updateRiderLocationOnSenderDoc(final Location location) {

        try {

        } catch (NullPointerException e) {
            Log.e(TAG, "updateRiderLocationOnSenderDoc: User instance is null, stopping location service.");
            Log.e(TAG, "updateRiderLocationOnSenderDoc: NullPointerException: " + e.getMessage());
            stopSelf();
        }

    }
}
