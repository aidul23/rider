package com.flyerz.rider.utility;

import android.graphics.Bitmap;
import android.location.Address;
import android.text.format.DateFormat;

import androidx.annotation.NonNull;

import com.flyerz.rider.callbacks.OnRiderRegistrationStatusCallback;
import com.flyerz.rider.constants.Constants;
import com.flyerz.rider.model.Rider;
import com.flyerz.rider.myapp.MyApp;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;

public class Utility {

    public static Rider rider;

    public static FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();

    public static CollectionReference riderCollectionReference =
            FirebaseFirestore.getInstance().collection("RIDER");

    public static Query getQuery(String s) {

        Query query = null;

        switch (s) {
            case "Delivered":
                query =
                        FirebaseFirestore.getInstance()
                                .collection("PARCEL")
                                .whereEqualTo("orderStatus", "Delivered")
                                .whereEqualTo("riderPhoneNumber", firebaseAuth.getCurrentUser().getPhoneNumber());;
                break;
            case "processing":
                query =
                        FirebaseFirestore.getInstance()
                                .collection("PARCEL")
                                .whereEqualTo("orderStatus", "Processing");
                break;
            case "pending confirmation":
                query =
                        FirebaseFirestore.getInstance()
                                .collection("PARCEL")
                                .whereEqualTo("orderStatus", "Pending Confirmation");
                break;

            case "Order Placed":
                query =
                        FirebaseFirestore.getInstance()
                                .collection("PARCEL")
                                .whereNotEqualTo("orderStatus", "Delivered")
                                .whereEqualTo("riderPhoneNumber", firebaseAuth.getCurrentUser().getPhoneNumber());
                break;
            default:
                query = FirebaseFirestore.getInstance()
                        .collection("PARCEL").limit(4);
        }
        return query;
    }

    public static void getRiderRegistrationStatus(OnRiderRegistrationStatusCallback callback) {
        riderCollectionReference
                .document(firebaseAuth.getCurrentUser().getPhoneNumber())
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        callback.isRiderRegistered(documentSnapshot.exists());
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull @NotNull Exception e) {
                callback.isRiderRegistered(false);
            }
        });

    }

    public static LatLng getLatLngFromAddress(String address) {
        LatLng latLng = null;
        try {
            List<Address> addresses = MyApp.getGeocoder().getFromLocationName(address, 1);
            if (addresses.size() > 0) {
                latLng = new LatLng(
                        addresses.get(0).getLatitude(), addresses.get(0).getLongitude()
                );
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return latLng;
    }

    public static String getDocumentIdGeneratedByFirestore() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        CollectionReference parcelCollectionRef = db.collection("PARCEL");
        return parcelCollectionRef.document().getId();
    }

    // Serialize a single object.
    public static String serializeToJson(Bitmap bmp) {
        Gson gson = new Gson();
        return gson.toJson(bmp);
    }

    // Deserialize to single object.
    public static Bitmap deserializeFromJson(String jsonString) {
        Gson gson = new Gson();
        return gson.fromJson(jsonString, Bitmap.class);
    }

    public static String formatMillisecondsIntoDateAndTime(long time) {
        if (time <= 0) return "";
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        return DateFormat.format("dd-MM-yyyy hh:mm aa", calendar).toString();
    }

    public static void setHasUserAcknowledgedApprovalToTrue() {
        MyApp.getSharedPreferences().edit().putBoolean(Constants.IS_USER_ACKNOWLEDGED_APPROVAL_KEY, true).apply();
    }

    public static boolean getHasUserAcknowledgedApproval() {
        return MyApp.getSharedPreferences().getBoolean(Constants.IS_USER_ACKNOWLEDGED_APPROVAL_KEY, false);
    }

    public static void clearSharedPreferences() {
        MyApp.getSharedPreferences().edit().clear().apply();
    }

}
