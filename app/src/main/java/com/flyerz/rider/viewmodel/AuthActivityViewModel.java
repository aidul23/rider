package com.flyerz.rider.viewmodel;

import androidx.lifecycle.ViewModel;

public class AuthActivityViewModel extends ViewModel {

    private String verificationCode;
    private String userPhoneNumber;

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    public String getUserPhoneNumber() {
        return userPhoneNumber;
    }

    public void setUserPhoneNumber(String userPhoneNumber) {
        this.userPhoneNumber = userPhoneNumber;
    }
}
