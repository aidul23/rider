package com.flyerz.rider.viewmodel;

import android.text.TextUtils;

import androidx.lifecycle.ViewModel;

import com.flyerz.rider.callbacks.OnRiderCallback;
import com.flyerz.rider.model.Rider;
import com.flyerz.rider.utility.Utility;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class DashboardActivityViewModel extends ViewModel {

    public void getRider(OnRiderCallback callback) {
        FirebaseFirestore.getInstance().collection("RIDER")
                .document(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber())
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if (documentSnapshot.exists()) {

                            Utility.rider = documentSnapshot.toObject(Rider.class);

                            callback.onRider(documentSnapshot.toObject(Rider.class));

                            callback.onRiderStatus(!Utility.rider.isHasRequestedToLeave()
                                    &&
                                    !TextUtils.equals(Utility.rider.getRiderStatus(), "Pending Confirmation"));

                            if (Utility.rider.isHasRequestedToLeave() || Utility.rider.isHasApprovedToLeave()) {
                                callback.onRiderAvailabilityRequest(
                                        Utility.rider.isHasRequestedToLeave(),
                                        Utility.rider.isHasApprovedToLeave()
                                );
                            }

                        }
                    }
                });
    }

}
