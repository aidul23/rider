package com.flyerz.rider.viewmodel;

import android.location.Location;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.flyerz.rider.model.LocationStatus;
import com.flyerz.rider.model.ParcelDeliveryOrder;
import com.flyerz.rider.model.ParcelOrderTrack;
import com.flyerz.rider.utility.Utility;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

public class MainActivityViewModel extends ViewModel {

    private static final String TAG = "MainActivityViewModel";
    private final MutableLiveData<Boolean> isDeliveryFinished;
    private final MutableLiveData<Boolean> isDeliveryStarted;

    public MainActivityViewModel() {
        isDeliveryFinished = new MutableLiveData<>();
        isDeliveryStarted = new MutableLiveData<>();
        isDeliveryFinished.setValue(false);
        isDeliveryStarted.setValue(false);
    }

    private final FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
    private ParcelOrderTrack parcelOrderTrack;

    private final MutableLiveData<ParcelDeliveryOrder> parcelDeliveryOrderMutableLiveData = new MutableLiveData<>();

    public MutableLiveData<ParcelDeliveryOrder> getParcelDeliveryOrderMutableLiveData() {
        return parcelDeliveryOrderMutableLiveData;
    }

    public void updateRiderLocationOnUserDocument(Location location) {

        LocationStatus locationStatus = new LocationStatus(String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()));

        try {
            firebaseFirestore.collection("PARCEL")
                    .document(getParcelDeliveryOrderMutableLiveData().getValue().getDocumentId())
                    .collection("rider location")
                    .document(getParcelDeliveryOrderMutableLiveData().getValue().getDocumentId())
                    .set(locationStatus);
        } catch (NullPointerException e) {
            Log.d(TAG, "updateRiderLocationOnUserDocument: " + e.getMessage());
        }
    }

    /*
     *
     * Operations happening at parcel -> document id -> order track
     *
     * */

    private final String riderPhoneNumber =
            FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber();

    public void setParcelOrderAccepted() {

        parcelOrderTrack = new ParcelOrderTrack(
                true, "Your order has been accepted", String.valueOf(System.currentTimeMillis()), "Order Accepted"
        );

        firebaseFirestore.collection("PARCEL")
                .document(getParcelDeliveryOrderMutableLiveData().getValue().getDocumentId())
                .collection("order track")
                .document(Utility.getDocumentIdGeneratedByFirestore())
                .set(parcelOrderTrack);

        setDeliveryStartedAtForRider();
        setParcelStatus("Accepted");
        getIsDeliveryStarted().setValue(true);
    }

    /*
     *
     * Operations happening at parcel -> document id -> order track
     *
     * */
    public void setParcelOrderPickedUp() {

        parcelOrderTrack = new ParcelOrderTrack(
                true, "Your parcel has been picked up by rider", String.valueOf(System.currentTimeMillis()), "Parcel Picked"
        );

        firebaseFirestore.collection("PARCEL")
                .document(getParcelDeliveryOrderMutableLiveData().getValue().getDocumentId())
                .collection("order track")
                .document(Utility.getDocumentIdGeneratedByFirestore())
                .set(parcelOrderTrack);

        setParcelStatus("Pickedup");
    }

    /*
     *
     * Operations happening at parcel -> document id -> order track
     *
     * */
    public void setParcelOrderOngoing() {

        parcelOrderTrack = new ParcelOrderTrack(
                true, "Parcel is being delivered to the destination", String.valueOf(System.currentTimeMillis()), "Being Delivered"
        );

        firebaseFirestore.collection("PARCEL")
                .document(getParcelDeliveryOrderMutableLiveData().getValue().getDocumentId())
                .collection("order track")
                .document(Utility.getDocumentIdGeneratedByFirestore())
                .set(parcelOrderTrack);

        setParcelStatus("Ongoing");
    }

    /*
     *
     * Operations happening at parcel -> document id -> order track
     *
     * */
    public void setParcelOrderDelivered() {

        parcelOrderTrack = new ParcelOrderTrack(
                true, "Your parcel has been delivered to the recipient", String.valueOf(System.currentTimeMillis()), "Parcel Delivered"
        );

        firebaseFirestore.collection("PARCEL")
                .document(getParcelDeliveryOrderMutableLiveData().getValue().getDocumentId())
                .collection("order track")
                .document(Utility.getDocumentIdGeneratedByFirestore())
                .set(parcelOrderTrack);

        setParcelStatus("Delivered");
    }

    /*
     *
     * Operations happening at
     * parcel -> document id -> order status
     * parcel -> document id -> rider details -> rider current status
     * rider -> rider phone number -> rider status
     *
     * */
    public void setParcelStatus(String status) {
        firebaseFirestore.collection("PARCEL")
                .document(getParcelDeliveryOrderMutableLiveData().getValue().getDocumentId())
                .update("orderStatus", status);

        firebaseFirestore.collection("PARCEL")
                .document(getParcelDeliveryOrderMutableLiveData().getValue().getDocumentId())
                .collection("rider details")
                .document(getParcelDeliveryOrderMutableLiveData().getValue().getDocumentId())
                .update("riderCurrentStatus", status);

        firebaseFirestore.collection("RIDER")
                .document(riderPhoneNumber)
                .update("riderStatus", status);
    }

    /*
     *
     * Operations happening at parcel -> document id -> rider details -> delivery started at
     *
     * */
    private void setDeliveryStartedAtForRider() {
        firebaseFirestore.collection("PARCEL")
                .document(getParcelDeliveryOrderMutableLiveData().getValue().getDocumentId())
                .collection("rider details")
                .document(getParcelDeliveryOrderMutableLiveData().getValue().getDocumentId())
                .update("deliveryStartedAt", String.valueOf(System.currentTimeMillis()));
    }


    /*
     *
     * Operations happening at parcel -> document id -> delivered at
     *
     * */
    public void setParcelOrderDeliveredAt() {
        firebaseFirestore.collection("PARCEL")
                .document(getParcelDeliveryOrderMutableLiveData().getValue().getDocumentId())
                .update("deliveredAt", String.valueOf(System.currentTimeMillis()));

        firebaseFirestore.collection("RIDER")
                .document(riderPhoneNumber)
                .update("riderStatus", "Available");

        getIsDeliveryFinished().setValue(true);
    }

    /*
     *
     * Operations happening at parcel -> document id -> rider details -> delivery finished at
     *
     * */
    public void setParcelOrderFinishedAt() {
        firebaseFirestore.collection("PARCEL")
                .document(getParcelDeliveryOrderMutableLiveData().getValue().getDocumentId())
                .collection("rider details")
                .document(getParcelDeliveryOrderMutableLiveData().getValue().getDocumentId())
                .update("deliveryFinishedAt", String.valueOf(System.currentTimeMillis()));
    }


    public MutableLiveData<Boolean> getIsDeliveryFinished() {
        return isDeliveryFinished;
    }

    public MutableLiveData<Boolean> getIsDeliveryStarted() {
        return isDeliveryStarted;
    }
}
