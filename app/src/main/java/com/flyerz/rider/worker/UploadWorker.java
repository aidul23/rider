package com.flyerz.rider.worker;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.flyerz.rider.constants.Constants;
import com.flyerz.rider.utility.Utility;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayOutputStream;

public class UploadWorker extends Worker {

    private final String phoneNumber = FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber();
    private static final String TAG = "UploadWorker";
    private StorageReference storageReference;
    private String whereToUpload;

    public UploadWorker(@NonNull @NotNull Context context, @NonNull @NotNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @NotNull
    @Override
    public Result doWork() {

        try {
            Data data = getInputData();

            whereToUpload = data.getString(Constants.UPLOAD_WORKER_KEY);

            String bitmapString = data.getString(Constants.BITMAP_STRING_KEY);

            Bitmap bitmap = Utility.deserializeFromJson(bitmapString);

            switch (whereToUpload) {

                case "profile":
                    storageReference = FirebaseStorage.getInstance().getReference().child(Constants.STORAGE_RIDER).child(phoneNumber + "/profile");
                    break;

                case "back":
                    storageReference = FirebaseStorage.getInstance().getReference().child(Constants.STORAGE_RIDER).child(phoneNumber + "/nidback");
                    break;

                case "front":
                    storageReference = FirebaseStorage.getInstance().getReference().child(Constants.STORAGE_RIDER).child(phoneNumber + "/nidfront");
                    break;

                case "passport":
                    storageReference = FirebaseStorage.getInstance().getReference().child(Constants.STORAGE_RIDER).child(phoneNumber + "/passport");
                    break;

                case "license_back":
                    storageReference = FirebaseStorage.getInstance().getReference().child(Constants.STORAGE_RIDER).child(phoneNumber + "/license_back");
                    break;

                case "license_front":
                    storageReference = FirebaseStorage.getInstance().getReference().child(Constants.STORAGE_RIDER).child(phoneNumber + "/license_front");
                    break;
            }

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 75, baos);
            byte[] bitmapData = baos.toByteArray();

            UploadTask task = storageReference.putBytes(bitmapData);

            task.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                    storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            setPhotoInDb(uri.toString());
                        }
                    });


                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull @NotNull Exception e) {
                    Log.d(TAG, "onFailure: " + e.getMessage());
                }
            });

        } catch (Exception e) {
            Log.d(TAG, "doWork: " + e.getMessage());
        }

        return Result.success();
    }

    private void setPhotoInDb(String url) {

        switch (whereToUpload) {
            case "profile":
                Utility.riderCollectionReference.document(phoneNumber).update("riderImage", url);
                break;
            case "back":
                Utility.riderCollectionReference.document(phoneNumber).collection("additional information").document(phoneNumber).update("riderNidBack", url);
                break;
            case "front":
                Utility.riderCollectionReference.document(phoneNumber).collection("additional information").document(phoneNumber).update("riderNidFront", url);
                break;
            case "passport":
                Utility.riderCollectionReference.document(phoneNumber).collection("additional information").document(phoneNumber).update("riderPassport", url);
                break;
            case "license_back":
                Utility.riderCollectionReference.document(phoneNumber).collection("additional information").document(phoneNumber).update("riderLicenseBack", url);
                break;
            case "license_front":
                Utility.riderCollectionReference.document(phoneNumber).collection("additional information").document(phoneNumber).update("riderLicenseFront", url);
                break;
        }

    }
}
